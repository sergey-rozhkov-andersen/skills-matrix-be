const connectToDb = require('./middleware/mongo.client');
const app = require('./app');
const { PORT } = require('./config');

process.on('unhandledRejection', reason => {
  process.emit('uncaughtException', reason);
});

connectToDb(() => {
  app.listen(PORT, () =>
    console.log(`App is running on http://localhost:${PORT}`)
  );
});
