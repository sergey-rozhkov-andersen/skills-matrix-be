const {
  NOT_FOUND,
  NO_CONTENT,
  CONFLICT,
  getStatusText
} = require('http-status-codes');
const responseStatus = require('./response-status');

const responseToClient = async (promise, req, res, model, next) => {
  promise
    .then(response => {
      if (!response) {
        return responseStatus(res, getStatusText(NOT_FOUND));
      }
      if (response === NO_CONTENT) {
        const deleteName = req.baseUrl
          .split('/')
          .slice(-1)[0]
          .slice(0, -1);
        return responseStatus(res, getStatusText(NO_CONTENT), deleteName);
      }
      if (response === CONFLICT) {
        return responseStatus(res, getStatusText(CONFLICT));
      }

      res.json(
        Array.isArray(response)
          ? response.map(model.toResponse)
          : model.toResponse(response)
      );
    })
    .catch(error => {
      return next(error);
    });
};

module.exports = responseToClient;
