const mongoose = require('mongoose');
const { MONGO_CONNECTION_STRING } = require('../config');

const connectToDb = callback => {
  mongoose
    .connect(MONGO_CONNECTION_STRING, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useUnifiedTopology: true
    })
    .then(() => {
      console.log('Connected');
    });

  mongoose.connection.on(
    'error',
    console.error.bind(console, 'connection error:')
  );
  mongoose.connection.once('open', () => {
    console.log('Connected to DB');
    callback();
  });
};

module.exports = connectToDb;
