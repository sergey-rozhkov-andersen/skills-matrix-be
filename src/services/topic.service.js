const { NOT_FOUND, OK } = require('http-status-codes');

const topicRepo = require('../repos/topic.repo');

const getAll = () => topicRepo.getAll();

const getTopic = async (req, res) => {
  const { id } = req.params;
  const topic = await topicRepo.getTopic(id);

  if (topic) {
    return res.status(OK).json(topic);
  }

  return res.status(NOT_FOUND).send();
};

const addTopic = async (req, res) => {
  const topic = await topicRepo.addTopic(req.body);

  return res.status(OK).json(topic);
};

const updateTopic = async (req, res) => {
  const { id } = req.params;
  const topic = await topicRepo.updateTopic(id, req.body);

  return res.status(OK).json(topic);
};

const deleteTopic = async (req, res) => {
  const { id } = req.params;
  const topic = topicRepo.deleteTopic(id);

  // delete subtopics

  return res.status(OK).json(topic);
};

module.exports = { getAll, getTopic, addTopic, updateTopic, deleteTopic };
