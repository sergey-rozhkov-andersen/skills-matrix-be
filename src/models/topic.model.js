const uuid = require('uuid');
const mongoose = require('mongoose');

const topicSchema = new mongoose.Schema(
  {
    _id: { type: String, default: uuid },
    parentId: { type: String, required: false },
    name: { type: String, required: true, trim: true },
    description: { type: String, required: true, trim: true }
    // relModel: {
    //   type: mongoose.Schema.ObjectId,
    //   ref: 'relModel',
    //   required: true
    // }
  },
  { versionKey: false }
);

const Topic = mongoose.model('Topic', topicSchema);

module.exports = Topic;
