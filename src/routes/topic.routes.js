const router = require('express').Router();
const topicsService = require('../services/topic.service');

router.route('/').get(topicsService.getAll);

router.route('/:id').get(topicsService.getTopic);

router.route('/').post(topicsService.addTopic);

router.route('/:id').put(topicsService.updateTopic);

router.route('/:id').delete(topicsService.deleteTopic);

module.exports = router;
