const Topic = require('../models/topic.model');

const getAll = async () => Topic.find({});

const getTopic = async id => Topic.findById(id);

const getAllByPatentId = async parentId => Topic.find({ parentId });

const addTopic = async topic => Topic.create(topic);

// const updateTopic = async (id, topic) => Topic.updateOne({ _id: topic.id }, topic);
const updateTopic = async (id, topic) => Topic.findByIdAndUpdate(id, topic);

// const deleteTopic = async id => Topic.deleteOne({ _id: id });
const deleteTopic = async id => Topic.findByIdAndDelete(id);

module.exports = {
  getAll,
  getTopic,
  getAllByPatentId,
  addTopic,
  updateTopic,
  deleteTopic
};
